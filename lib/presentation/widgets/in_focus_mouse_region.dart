import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class InFocusMouseRegion extends StatelessWidget {
  const InFocusMouseRegion({
    Key? key,
    required this.child,
    this.inFocus,
  }) : super(key: key);

  final Widget child;
  final Function(bool inFocus)? inFocus;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (PointerEnterEvent event) {
        if (inFocus != null) {
          inFocus!(true);
        }
      },
      onExit: (PointerExitEvent event) {
        if (inFocus != null) {
          inFocus!(false);
        }
      },
      child: child,
    );
  }
}
