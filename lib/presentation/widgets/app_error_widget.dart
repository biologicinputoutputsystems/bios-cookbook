import 'package:bios_cookbook/bios_cookbook.dart';
import 'package:flutter/material.dart';

class AppErrorWidget extends StatelessWidget {
  const AppErrorWidget(
    this.appError, {
    Key? key,
    this.action,
    this.onClose,
  }) : super(key: key);

  final AppError appError;
  final VoidCallback? action;
  final VoidCallback? onClose;

  @override
  Widget build(BuildContext context) {
    // if (appError is UnauthorizedException) {
    //   // todo
    //   // if (Shared.of(context).onUnauthorizedError != null) {
    //   //   Shared.of(context).onUnauthorizedError!();
    //   // }
    //   return;
    // }

    final bool hasAction = action != null;
    final bool withClose = onClose != null;

    final String title = appError.title.isNotNullEmptyOrWhitespace
        ? appError.title!
        : Cookbook.of(context).config.strings.unknownErrorTitle;

    final String description = appError.message.isNotNullEmptyOrWhitespace
        ? appError.message
        : Cookbook.of(context).config.strings.unknownErrorDescription;
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: Dimens.margin5),
          // SvgPicture.asset(
          //   Shared.of(context).config.assets.errorPlaceholder,
          // ),
          // const SizedBox(height: Dimens.margin_7),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Text(
                  title.toUpperCase(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.displayMedium?.copyWith(
                        color: Colors.black,
                      ),
                ),
              ),
            ],
          ),
          const SizedBox(height: Dimens.margin5),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Text(
                  description,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                        color: Colors.black.withOpacity(0.4),
                      ),
                ),
              ),
            ],
          ),
          if (hasAction) const SizedBox(height: Dimens.margin5),
          if (hasAction)
            PrimaryButton(
              title: Cookbook.of(context).config.strings.unknownErrorAction,
              onPressed: () {
                action!();
              },
            ),
          if (withClose) const SizedBox(height: Dimens.margin3),
          if (withClose)
            SecondaryButton(
              title: Cookbook.of(context).config.strings.close,
              color: AppColors.secondary,
              onPressed: () {
                Navigator.of(context).pop();
                if (onClose != null) {
                  onClose!();
                }
              },
            ),
          const SizedBox(height: Dimens.margin5),
        ],
      ),
    );
  }
}
