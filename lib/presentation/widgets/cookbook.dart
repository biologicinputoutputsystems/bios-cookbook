import 'package:bios_cookbook/utils/cookbook_config.dart';
import 'package:flutter/material.dart';

class Cookbook extends InheritedWidget {
  const Cookbook({
    required this.config,
    this.onUnauthorizedError,
    required Widget child,
  }) : super(child: child);

  static Cookbook of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<Cookbook>()!;
  }

  final CookbookConfig config;
  final Function()? onUnauthorizedError;

  @override
  bool updateShouldNotify(Cookbook oldWidget) => config != oldWidget.config;
}
