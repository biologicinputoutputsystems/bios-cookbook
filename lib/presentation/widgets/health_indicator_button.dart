import 'package:bios_cookbook/utils/theme/colors.dart';
import 'package:bios_cookbook/utils/theme/dimens.dart';
import 'package:flutter/material.dart';

enum HealthIndicatorState { success, warning, error, none }

class HealthIndicatorButton extends StatelessWidget {
  const HealthIndicatorButton({
    Key? key,
    required this.icon,
    required this.title,
    required this.state,
    this.onPressed,
    this.selected = false,
  }) : super(key: key);

  final IconData icon;
  final String title;
  final HealthIndicatorState state;
  final VoidCallback? onPressed;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return Material(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      elevation: 1.0,
      color: selected ? AppColors.buttonSelected : Colors.white,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
        child: InkWell(
          onTap: onPressed,
          child: Stack(
            children: [
              Container(
                padding: const EdgeInsets.all(Dimens.margin4),
                child: Row(
                  children: <Widget>[
                    Icon(
                      icon,
                      size: 24.0,
                      color: AppColors.primary.shade100,
                    ),
                    const SizedBox(width: Dimens.margin4),
                    Expanded(
                      child: Text(
                        title,
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(
                              color: selected ? Colors.white : AppColors.text,
                            ),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                right: 0.0,
                top: 0.0,
                bottom: 0.0,
                child: Container(
                  width: 6.0,
                  color: _getIndicatorColor(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Color _getIndicatorColor() {
    switch (state) {
      case HealthIndicatorState.success:
        return AppColors.success;
      case HealthIndicatorState.warning:
        return AppColors.warning;
      case HealthIndicatorState.error:
        return AppColors.error;
      case HealthIndicatorState.none:
        return AppColors.none;
    }
  }
}
