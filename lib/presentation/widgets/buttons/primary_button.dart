import 'package:bios_cookbook/utils/theme/colors.dart';
import 'package:bios_cookbook/utils/theme/dimens.dart';
import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  const PrimaryButton({
    Key? key,
    required this.title,
    this.onPressed,
    this.padding,
    this.margin,
    this.icon,
  }) : super(key: key);

  final String title;
  final VoidCallback? onPressed;
  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final Widget? icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: margin,
      child: ElevatedButton(
        child: Padding(
          padding: padding ??
              const EdgeInsets.symmetric(
                vertical: Dimens.margin2,
              ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                title.toUpperCase(),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: Colors.white.withOpacity(
                    onPressed != null ? 1.0 : 0.5,
                  ),
                ),
              ),
              if (icon != null) const SizedBox(width: Dimens.margin2),
              if (icon != null) icon!,
            ],
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(
            AppColors.primary.shade400.withOpacity(
              onPressed != null ? 1.0 : 0.5,
            ),
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
