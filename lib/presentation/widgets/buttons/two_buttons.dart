import 'package:bios_cookbook/presentation/widgets/buttons/primary_button.dart';
import 'package:bios_cookbook/presentation/widgets/buttons/secondary_button.dart';
import 'package:bios_cookbook/utils/theme/dimens.dart';
import 'package:flutter/material.dart';

class TwoButtons extends StatelessWidget {
  const TwoButtons({
    Key? key,
    required this.primaryTitle,
    required this.onPrimaryPressed,
    required this.secondaryTitle,
    required this.onSecondaryPressed,
    this.padding,
  }) : super(key: key);

  final String primaryTitle;
  final VoidCallback? onPrimaryPressed;
  final String secondaryTitle;
  final VoidCallback? onSecondaryPressed;
  final EdgeInsets? padding;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding ??
          const EdgeInsets.symmetric(
            horizontal: Dimens.margin3,
            vertical: Dimens.margin2,
          ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: SecondaryButton(
              title: secondaryTitle,
              onPressed: onSecondaryPressed,
            ),
          ),
          const SizedBox(width: Dimens.margin2),
          Expanded(
            child: PrimaryButton(
              title: primaryTitle,
              onPressed: onPrimaryPressed,
            ),
          ),
        ],
      ),
    );
  }
}
