import 'package:bios_cookbook/utils/theme/colors.dart';
import 'package:bios_cookbook/utils/theme/dimens.dart';
import 'package:flutter/material.dart';

class ListTabButton extends StatelessWidget {
  const ListTabButton({
    super.key,
    required this.title,
    this.onPressed,
    this.selected = false,
    this.indicatorColor,
  });

  final String title;
  final bool selected;
  final VoidCallback? onPressed;
  final Color? indicatorColor;

  @override
  Widget build(BuildContext context) {
    final BorderRadius borderRadius = BorderRadius.only(
      topLeft: const Radius.circular(Dimens.cornerRadius),
      bottomLeft: const Radius.circular(Dimens.cornerRadius),
      topRight:
          selected ? Radius.zero : const Radius.circular(Dimens.cornerRadius),
      bottomRight:
          selected ? Radius.zero : const Radius.circular(Dimens.cornerRadius),
    );
    return Padding(
      padding: EdgeInsets.only(
        right: selected ? 0.0 : Dimens.margin4,
      ),
      child: Material(
        shape: RoundedRectangleBorder(
          borderRadius: borderRadius,
        ),
        elevation: 1.0,
        color: selected ? Colors.white : AppColors.secondary.shade400,
        child: ClipRRect(
          borderRadius: borderRadius,
          child: InkWell(
            onTap: onPressed,
            child: Stack(
              children: [
                Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: Dimens.margin4,
                    vertical: Dimens.margin2,
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style:
                              Theme.of(context).textTheme.bodyMedium?.copyWith(
                                    fontWeight: FontWeight.w600,
                                    color: selected
                                        ? AppColors.buttonSelected
                                        : Colors.white,
                                  ),
                        ),
                      ),
                    ],
                  ),
                ),
                if (indicatorColor != null)
                  Positioned(
                    right: 0.0,
                    top: 0.0,
                    bottom: 0.0,
                    child: Container(
                      width: 6.0,
                      color: indicatorColor,
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }

//
// @override
// Widget build(BuildContext context) {
//   return Padding(
//     padding: EdgeInsets.only(
//       right: selected ? 0.0 : Dimens.margin4,
//     ),
//     child: ElevatedButton(
//       child: Container(
//         padding: const EdgeInsets.symmetric(vertical: Dimens.margin2),
//         child: Row(
//           children: [
//             Text(
//               title,
//               maxLines: 1,
//               overflow: TextOverflow.ellipsis,
//               style: Theme.of(context).textTheme.bodyText2?.copyWith(
//                     fontWeight: FontWeight.w600,
//                     color: selected ? AppColors.buttonSelected : Colors.white,
//                   ),
//             ),
//           ],
//         ),
//       ),
//       style: ButtonStyle(
//         backgroundColor: MaterialStateProperty.all(
//           selected ? Colors.white : AppColors.secondary.shade400,
//         ),
//       ),
//       onPressed: onPressed,
//     ),
//   );
// }
}
