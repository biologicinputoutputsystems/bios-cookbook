import 'axis.dart';

class BoneRotationData {
  BoneRotationData(this.name, [this.rotationX = 0, this.rotationY = 0, this.rotationZ = 0]);

  BoneRotationData.fromTargetAxis(this.name, Axis targetAxis, double value) {
    rotationX = targetAxis == Axis.X ? value : 0;
    rotationY = targetAxis == Axis.Y ? value : 0;
    rotationZ = targetAxis == Axis.Z ? value : 0;
  }

  final String name;
  late double rotationX;
  late double rotationY;
  late double rotationZ;

  Map<String, dynamic> toJson() => <String, dynamic>{
        'Name': name,
        'RotationX': rotationX,
        'RotationY': rotationY,
        'RotationZ': rotationZ,
      };

  @override
  bool operator ==(Object other) {
    if (other is! BoneRotationData) {
      return false;
    }

    return name == other.name &&
        rotationX == other.rotationX &&
        rotationY == other.rotationY &&
        rotationZ == other.rotationZ;
  }
}
