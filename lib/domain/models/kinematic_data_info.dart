import 'package:bios_cookbook/data/fs/csv/csv_file.dart';
import 'package:bios_cookbook/data/fs/csv/csv_file_interface.dart';

class KinematicDataInfo {
  const KinematicDataInfo(
    this.length,
    this.durationInSeconds,
  );

  factory KinematicDataInfo.fromCsvFile(String csvFilePath) {
    final ICsvFile<double> kinematicData = CsvFile<double>(csvFilePath);
    kinematicData.loadSync();

    final int length = kinematicData.rowCount - 1;
    final double durationInSeconds = kinematicData[length].first;

    return KinematicDataInfo(length, durationInSeconds);
  }

  final int length;
  final double durationInSeconds;

  int get durationInMilliseconds => (durationInSeconds * 1000).toInt();
}
