abstract class AppError implements Exception {
  String? get title;

  String get message;

  List<DetailsException>? get details;
}

abstract class DetailsException implements Exception {
  String get target;

  String get message;
}
