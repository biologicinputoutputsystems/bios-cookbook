import 'app_error.dart';

class UnknownException implements AppError {
  @override
  String? get title => 'Error occurred';

  @override
  String get message => 'Something went wrong';

  @override
  List<DetailsException>? get details => null;
}

class ConnectionException implements AppError {
  @override
  String? get title => 'Connection exception';

  @override
  String get message => 'There is no connection';

  @override
  List<DetailsException>? get details => null;
}

class UnauthorizedException implements AppError {
  @override
  String? get title => 'Token expired';

  @override
  String get message => 'Your token expired';

  @override
  List<DetailsException>? get details => null;
}

class SilentException implements AppError {
  @override
  String? get title => '';

  @override
  String get message => '';

  @override
  List<DetailsException>? get details => null;
}

class ValidationException implements AppError {
  ValidationException({
    required this.title,
    required this.message,
  });

  @override
  final String title;

  @override
  final String message;

  @override
  List<DetailsException>? get details => null;
}

class ClientDetailsException implements DetailsException {
  const ClientDetailsException({
    this.code = -1,
    this.target = '',
    this.message = '',
  });

  final int code;
  @override
  final String target;
  @override
  final String message;
}

class CompositeException implements Exception {
  const CompositeException({
    this.exceptions = const <Exception>[],
    this.message = 'Error occured',
    this.cause,
  });

  final List<Exception> exceptions;
  final String message;
  final Exception? cause;
}
