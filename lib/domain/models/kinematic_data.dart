import 'package:bios_cookbook/domain/models/bone_rotation_data.dart';

class KinematicData {
  const KinematicData(
    this.rotationData,
    this.progress,
    this.durationInSeconds,
    this.movementPosition,
  );

  final Iterable<BoneRotationData> rotationData;
  final double progress;
  final double durationInSeconds;
  final int movementPosition;
}
