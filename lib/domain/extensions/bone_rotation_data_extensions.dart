import 'package:bios_cookbook/domain/models/bone_rotation_data.dart';

extension BoneRotationDataExtensions on Iterable<BoneRotationData> {
  Iterable<BoneRotationData> joinByBoneName() {
    final Set<String> uniqueBoneNames = map((BoneRotationData e) => e.name).toSet();
    final List<BoneRotationData> result = <BoneRotationData>[];

    for (final String boneName in uniqueBoneNames) {
      final BoneRotationData boneRotationData = BoneRotationData(boneName);

      final Iterable<BoneRotationData> bonesRotationDataForName =
          where((BoneRotationData element) => element.name == boneName);

      for (final BoneRotationData boneRotationDataForName in bonesRotationDataForName) {
        boneRotationData.rotationX += boneRotationDataForName.rotationX;
        boneRotationData.rotationY += boneRotationDataForName.rotationY;
        boneRotationData.rotationZ += boneRotationDataForName.rotationZ;
      }

      result.add(boneRotationData);
    }

    return result;
  }
}
