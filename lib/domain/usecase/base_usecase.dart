library usecase;

export 'base_future_usecase.dart';
export 'base_stream_usecase.dart';

abstract class BaseUseCase<Param, T> {
  const BaseUseCase();

  T execute(Param param);
}

abstract class BaseNoParamUseCase<T> {
  const BaseNoParamUseCase();

  T execute();
}
