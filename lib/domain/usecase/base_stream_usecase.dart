import 'base_usecase.dart';

abstract class BaseStreamUseCase<Param, T>
    extends BaseUseCase<Param, Stream<T>> {
  const BaseStreamUseCase();
}

abstract class BaseNoParamStreamUseCase<T>
    extends BaseNoParamUseCase<Stream<T>> {
  const BaseNoParamStreamUseCase();
}
