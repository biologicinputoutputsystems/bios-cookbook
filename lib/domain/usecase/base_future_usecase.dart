import 'base_usecase.dart';

abstract class BaseFutureUseCase<Param, T>
    extends BaseUseCase<Param, Future<T>> {
  const BaseFutureUseCase();
}

abstract class BaseNoParamFutureUseCase<T>
    extends BaseNoParamUseCase<Future<T>> {
  const BaseNoParamFutureUseCase();
}
