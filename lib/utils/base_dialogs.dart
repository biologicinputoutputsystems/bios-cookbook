import 'package:bios_cookbook/domain/models/app_error.dart';
import 'package:bios_cookbook/domain/models/exceptions.dart';
import 'package:bios_cookbook/presentation/presentation.dart';
import 'package:bios_cookbook/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart' as toast;

extension BaseDialogs on NavigatorState {
  Future<void> showLoadingDialog({
    bool dismissible = false,
    bool withBackground = false,
  }) {
    return showDialog<void>(
      context: context,
      barrierDismissible: dismissible,
      builder: (BuildContext context) => WillPopScope(
        onWillPop: () {
          if (dismissible) {
            pop();
          }
          return Future<bool>.value(false);
        },
        child: Container(
          child: const Center(
            child: CircularProgressIndicator(),
          ),
          color: withBackground ? Colors.white : null,
        ),
      ),
    );
  }

  Future<T?> showInfoDialog<T extends Object>({
    required String title,
    required String description,
    required String primaryCta,
    VoidCallback? onPrimary,
    String? secondaryCta,
    VoidCallback? onSecondary,
    bool dismissible = true,
  }) =>
      showInfoTemplateDialog<T>(
        title: Text(
          title,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                color: AppColors.primary.shade900,
              ),
        ),
        description: Text(
          description,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                color: Colors.black,
              ),
        ),
        primaryCta: primaryCta,
        onPrimary: onPrimary,
        secondaryCta: secondaryCta,
        onSecondary: onSecondary,
        dismissible: dismissible,
      );

  Future<void> showActionDialog({
    bool dismissible = true,
    required String title,
    required String description,
    VoidCallback? onPositive,
    String? positiveText,
    VoidCallback? onNegative,
    String? negativeText,
    VoidCallback? onCancel,
  }) {
    return showTemplateDialog(
      dismissible: dismissible,
      children: <Widget>[
        const SizedBox(height: Dimens.margin5),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Text(
                title.toUpperCase(),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.displayMedium?.copyWith(
                      color: Colors.black,
                    ),
              ),
            ),
          ],
        ),
        const SizedBox(height: Dimens.margin5),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Text(
                description,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                      color: Colors.black.withOpacity(0.4),
                    ),
              ),
            ),
          ],
        ),
        const SizedBox(height: Dimens.margin5),
        TwoButtons(
          primaryTitle:
              positiveText ?? Cookbook.of(context).config.strings.apply,
          onPrimaryPressed: () {
            pop();
            if (onPositive != null) {
              onPositive();
            }
          },
          secondaryTitle:
              negativeText ?? Cookbook.of(context).config.strings.cancel,
          onSecondaryPressed: () {
            pop();
            if (onNegative != null) {
              onNegative();
            }
          },
        ),
        const SizedBox(height: Dimens.margin5),
      ],
    ).then((Object? value) {
      if (value == null && onCancel != null) {
        onCancel();
      }
    });
  }

  Future<T?> showTemplateDialog<T extends Object>({
    List<Widget>? children,
    double padding = 16.0,
    bool dismissible = true,
  }) =>
      showDialog<T>(
        context: context,
        barrierDismissible: dismissible,
        builder: (BuildContext context) => SimpleDialog(
          contentPadding: EdgeInsets.symmetric(
            horizontal: padding,
            vertical: padding,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          ),
          children: children,
        ),
      );

  Future<T?> showInfoTemplateDialog<T extends Object>({
    required Widget title,
    required Widget description,
    required String primaryCta,
    VoidCallback? onPrimary,
    String? secondaryCta,
    VoidCallback? onSecondary,
    bool dismissible = true,
  }) =>
      showTemplateDialog<T>(
        dismissible: dismissible,
        children: <Widget>[
          const SizedBox(height: Dimens.margin5),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: title,
              ),
            ],
          ),
          const SizedBox(height: Dimens.margin5),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: description,
              ),
            ],
          ),
          const SizedBox(height: Dimens.margin5),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              PrimaryButton(
                title: primaryCta,
                onPressed: () {
                  pop();
                  if (onPrimary != null) {
                    onPrimary();
                  }
                },
              ),
            ],
          ),
          if (onSecondary != null) const SizedBox(height: Dimens.margin3),
          if (onSecondary != null)
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SecondaryButton(
                  title: secondaryCta!,
                  color: AppColors.secondary,
                  onPressed: () {
                    pop();
                    onSecondary();
                  },
                ),
              ],
            ),
          const SizedBox(height: Dimens.margin5),
        ],
      );

  void showConnectionErrorDialog({
    Function? action,
    VoidCallback? onClose,
    bool dismissible = false,
  }) {
    final bool hasAction = action != null;
    showTemplateDialog(
      dismissible: dismissible,
      children: <Widget>[
        const SizedBox(height: Dimens.margin5),
        // SvgPicture.asset(
        //   Shared.of(context).config.assets.connectionPlaceholder,
        // ),
        // const SizedBox(height: Dimens.margin_7),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Text(
                Cookbook.of(context).config.strings.connectionErrorTitle,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.displayMedium?.copyWith(
                      color: Colors.black,
                    ),
              ),
            ),
          ],
        ),
        const SizedBox(height: Dimens.margin5),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Text(
                Cookbook.of(context).config.strings.connectionErrorDescription,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                      color: Colors.black.withOpacity(0.4),
                    ),
              ),
            ),
          ],
        ),
        if (hasAction) const SizedBox(height: Dimens.margin5),
        if (hasAction)
          PrimaryButton(
            title: Cookbook.of(context).config.strings.connectionErrorAction,
            onPressed: () {
              pop();
              action();
            },
          ),
        const SizedBox(height: Dimens.margin3),
        SecondaryButton(
          title: Cookbook.of(context).config.strings.close,
          color: AppColors.secondary,
          onPressed: () {
            pop();
            if (onClose != null) {
              onClose();
            }
          },
        ),
        const SizedBox(height: Dimens.margin5),
      ],
    );
  }

  void maybeShowConnectionErrorDialog({
    Function? action,
    VoidCallback? onClose,
    bool hasConnection = false,
  }) {
    if (!hasConnection) {
      showConnectionErrorDialog(
        action: action,
        onClose: onClose,
      );
    } else if (action != null) {
      action();
    }
  }

  void showAppErrorDialog(
    AppError error, {
    Function? action,
    VoidCallback? onClose,
    bool dismissible = true,
  }) {
    if (error is UnauthorizedException) {
      if (Cookbook.of(context).onUnauthorizedError != null) {
        Cookbook.of(context).onUnauthorizedError!();
      }
      return;
    }
    if (error is SilentException) {
      return;
    }

    if (error is ConnectionException) {
      showConnectionErrorDialog(
        action: action,
        onClose: onClose,
      );
    } else {
      showErrorDialog(
        appError: error,
        action: action,
        onClose: onClose,
        dismissible: dismissible,
      );
    }
  }

  void showErrorDialog({
    Function? action,
    VoidCallback? onClose,
    required AppError appError,
    bool dismissible = true,
  }) {
    if (appError is UnauthorizedException) {
      if (Cookbook.of(context).onUnauthorizedError != null) {
        Cookbook.of(context).onUnauthorizedError!();
      }
      return;
    }
    if (appError is SilentException) {
      return;
    }

    final bool hasAction = action != null;

    final String title = appError.title.isNotNullEmptyOrWhitespace
        ? appError.title!
        : Cookbook.of(context).config.strings.unknownErrorTitle;

    final String description = appError.message.isNotNullEmptyOrWhitespace
        ? appError.message
        : Cookbook.of(context).config.strings.unknownErrorDescription;

    showTemplateDialog(
      dismissible: dismissible,
      children: <Widget>[
        const SizedBox(height: Dimens.margin5),
        // SvgPicture.asset(
        //   Shared.of(context).config.assets.errorPlaceholder,
        // ),
        // const SizedBox(height: Dimens.margin_7),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Text(
                title.toUpperCase(),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.displayMedium?.copyWith(
                      color: Colors.black,
                    ),
              ),
            ),
          ],
        ),
        const SizedBox(height: Dimens.margin5),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Text(
                description,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                      color: Colors.black.withOpacity(0.4),
                    ),
              ),
            ),
          ],
        ),
        if (hasAction) const SizedBox(height: Dimens.margin5),
        if (hasAction)
          PrimaryButton(
            title: Cookbook.of(context).config.strings.unknownErrorAction,
            onPressed: () {
              pop();
              action();
            },
          ),
        const SizedBox(height: Dimens.margin3),
        SecondaryButton(
          title: Cookbook.of(context).config.strings.close,
          color: AppColors.secondary,
          onPressed: () {
            pop();
            if (onClose != null) {
              onClose();
            }
          },
        ),
        const SizedBox(height: Dimens.margin5),
      ],
    );
  }

  void showToast(
    BuildContext context,
    String message, {
    Color textColor = Colors.white,
    Color backgroundColor = Colors.black,
    toast.StyledToastPosition? position,
  }) =>
      toast.showToast(
        message,
        context: context,
        position: position,
        animation: toast.StyledToastAnimation.fade,
        reverseAnimation: toast.StyledToastAnimation.fade,
      );
}
