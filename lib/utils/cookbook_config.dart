abstract class CookbookConfig {
  CookbookStrings get strings;
}

abstract class CookbookStrings {
  String get connectionErrorTitle;

  String get connectionErrorDescription;

  String get connectionErrorAction;

  String get emptyErrorTitle;

  String get emptyErrorAction;

  String get unknownErrorTitle;

  String get unknownErrorDescription;

  String get unknownErrorAction;

  String get apply;

  String get cancel;

  String get close;

  String get backToHome;

  String get tryAgain;
}
