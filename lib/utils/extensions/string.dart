extension StringExtentions on String? {
  String get capitalize {
    if (isNullEmptyOrWhitespace) {
      return '';
    }

    return '${this![0].toUpperCase()}${this?.substring(1).toLowerCase()}';
  }

  String get toSnakeCase {
    if (isNullEmptyOrWhitespace) {
      return '';
    }

    return this?.trim().replaceAll(' ', '_') ?? '';
  }

  String get twoLetters {
    if (isNullEmptyOrWhitespace) {
      return '';
    }

    if ((this?.length ?? 0) < 2) {
      return this?.substring(0, 1) ?? '';
    } else {
      return this?.substring(0, 2) ?? '';
    }
  }

  /// Returns true if string is:
  /// - null
  /// - empty
  /// - whitespace string.
  ///
  /// Characters considered "whitespace" are listed [here](https://stackoverflow.com/a/59826129/10830091).
  bool get isNullEmptyOrWhitespace =>
      this == null || (this?.isEmpty ?? true) || (this?.trim().isEmpty ?? true);

  bool get isNotNullEmptyOrWhitespace => !isNullEmptyOrWhitespace;

  bool get isNumeric {
    if (isNullEmptyOrWhitespace) {
      return false;
    }
    return double.tryParse(this!) != null;
  }

  String interpolate(List<String> params) {
    if (this == null) {
      return '';
    }
    String result = this!;
    for (int i = 1; i < params.length + 1; i++) {
      result = result.replaceAll('%$i\$', params[i - 1]);
    }

    return result;
  }
}
