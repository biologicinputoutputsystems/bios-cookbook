import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

Logger logger = Logger(
  filter: kDebugMode ? DevelopmentFilter() : ProductionFilter()
    ..level = Level.nothing,
);
