import 'package:flutter/material.dart';

extension BaseNavigation on NavigatorState {
  Route<T> toRoute<T extends Object>(
    Widget screen, {
    String? routeName,
    bool fullscreenDialog = false,
    bool maintainState = true,
  }) {
    return MaterialPageRoute<T>(
      builder: (BuildContext context) => screen,
      settings: RouteSettings(name: routeName),
      fullscreenDialog: fullscreenDialog,
      maintainState: maintainState,
    );
  }

  Route<T> toInstantRoute<T extends Object>(
    Widget screen, {
    String? routeName,
    bool fullscreenDialog = false,
    bool maintainState = true,
  }) {
    return InstantRoute<T>(
      page: screen,
      settings: RouteSettings(name: routeName),
      fullscreenDialog: fullscreenDialog,
      maintainState: maintainState,
    );
  }

  Future<T?> openScreen<T extends Object>(
    Widget screen, {
    String? routeName,
    bool withReplacement = false,
    bool fullscreenDialog = false,
    bool maintainState = true,
  }) {
    if (withReplacement) {
      return pushReplacement(
        toRoute<T>(
          screen,
          routeName: routeName,
          fullscreenDialog: fullscreenDialog,
          maintainState: maintainState,
        ),
      );
    } else {
      return push(
        toRoute<T>(
          screen,
          routeName: routeName,
          fullscreenDialog: fullscreenDialog,
          maintainState: maintainState,
        ),
      );
    }
  }

  Future<T?> openScreenInstant<T extends Object>(
    Widget screen, {
    String? routeName,
    bool withReplacement = false,
    bool fullscreenDialog = false,
    bool maintainState = true,
  }) {
    if (withReplacement) {
      return pushReplacement(
        InstantRoute<T>(
          page: screen,
          settings: RouteSettings(name: routeName),
          fullscreenDialog: fullscreenDialog,
          maintainState: maintainState,
        ),
      );
    } else {
      return push(
        InstantRoute<T>(
          page: screen,
          settings: RouteSettings(name: routeName),
          fullscreenDialog: fullscreenDialog,
          maintainState: maintainState,
        ),
      );
    }
  }

  Future<T?> openScreenFade<T extends Object>(
    Widget screen, {
    String? routeName,
    bool withReplacement = false,
    bool fullscreenDialog = false,
    bool maintainState = true,
  }) {
    if (withReplacement) {
      return pushReplacement(
        FadeRoute<T>(
          page: screen,
          settings: RouteSettings(name: routeName),
          fullscreenDialog: fullscreenDialog,
          maintainState: maintainState,
        ),
      );
    } else {
      return push(
        FadeRoute<T>(
          page: screen,
          settings: RouteSettings(name: routeName),
          fullscreenDialog: fullscreenDialog,
          maintainState: maintainState,
        ),
      );
    }
  }

  Future<T?> openScreenSlideRight<T extends Object>(
    Widget screen, {
    String? routeName,
    bool withReplacement = false,
    bool fullscreenDialog = false,
    bool maintainState = true,
  }) {
    if (withReplacement) {
      return pushReplacement(
        SlideRightRoute<T>(
          page: screen,
          settings: RouteSettings(name: routeName),
          fullscreenDialog: fullscreenDialog,
          maintainState: maintainState,
        ),
      );
    } else {
      return push(
        SlideRightRoute<T>(
          page: screen,
          settings: RouteSettings(name: routeName),
          fullscreenDialog: fullscreenDialog,
          maintainState: maintainState,
        ),
      );
    }
  }

  Future<T?> pushFade<T extends Object>(Widget screen) => push(
        FadeRoute<T>(
          page: screen,
        ),
      );

  bool isGivenRouteSameAsCurrentRoute(String? routeName) {
    bool isNewRouteSameAsCurrent = false;

    popUntil((Route<dynamic> route) {
      if (route.settings.name == routeName) {
        isNewRouteSameAsCurrent = true;
      }
      return true;
    });

    return isNewRouteSameAsCurrent;
  }

  Future<void> openWithoutDuplicates(
    Widget screen, {
    String? routeName,
    bool fullscreenDialog = false,
    bool maintainState = true,
  }) {
    if (isGivenRouteSameAsCurrentRoute(routeName)) {
      return openScreen(
        screen,
        routeName: routeName,
        withReplacement: true,
        fullscreenDialog: fullscreenDialog,
        maintainState: maintainState,
      );
    } else {
      return openScreen(
        screen,
        routeName: routeName,
        fullscreenDialog: fullscreenDialog,
        maintainState: maintainState,
      );
    }
  }

  bool isCurrentRouteIsPopup() {
    bool isCurrentRouteIsPopup = false;

    popUntil((Route<dynamic> route) {
      if (route is PopupRoute) {
        isCurrentRouteIsPopup = true;
      }
      return true;
    });

    return isCurrentRouteIsPopup;
  }

  void openScreenNamed(
    String screen, {
    bool withReplacement = false,
    Object? arguments,
  }) {
    if (withReplacement) {
      pushReplacementNamed(
        screen,
        arguments: arguments,
      );
    } else {
      pushNamed(
        screen,
        arguments: arguments,
      );
    }
  }
}

class InstantRoute<T> extends PageRouteBuilder<T> {
  InstantRoute({
    this.page,
    bool fullscreenDialog = false,
    bool maintainState = true,
    RouteSettings? settings,
  }) : super(
          settings: settings,
          maintainState: maintainState,
          fullscreenDialog: fullscreenDialog,
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page!,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              child,
        );

  final Widget? page;
}

class FadeRoute<T> extends PageRouteBuilder<T> {
  FadeRoute({
    this.page,
    bool fullscreenDialog = false,
    bool maintainState = true,
    RouteSettings? settings,
  }) : super(
          settings: settings,
          maintainState: maintainState,
          fullscreenDialog: fullscreenDialog,
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page!,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
        );

  final Widget? page;
}

class EnterExitRoute<T> extends PageRouteBuilder<T> {
  EnterExitRoute({
    this.exitPage,
    this.enterPage,
  }) : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              enterPage!,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              Stack(
            children: <Widget>[
              SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(0.0, 0.0),
                  end: const Offset(-1.0, 0.0),
                ).animate(animation),
                child: exitPage,
              ),
              SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(1.0, 0.0),
                  end: Offset.zero,
                ).animate(animation),
                child: enterPage,
              )
            ],
          ),
        );

  final Widget? enterPage;
  final Widget? exitPage;
}

class SlideRightRoute<T> extends PageRouteBuilder<T> {
  SlideRightRoute({
    this.page,
    bool fullscreenDialog = false,
    bool maintainState = true,
    RouteSettings? settings,
  }) : super(
          settings: settings,
          maintainState: maintainState,
          fullscreenDialog: fullscreenDialog,
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page!,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(1, 0),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          ),
        );

  final Widget? page;
}
