import 'dart:async';
import 'dart:io';

class ConnectionUtils {
  ConnectionUtils() {
    _connectionChangeController = StreamController<bool?>.broadcast();
    _initialize();
  }

  bool _connected = false;
  late StreamController<bool?> _connectionChangeController;

  void _initialize() {
    // _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
    //   isConnected();
    // });
  }

  Future<bool> isConnected() async {
    final bool previousConnection = _connected;

    try {
      final List<InternetAddress> result =
          await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        _connected = true;
      } else {
        _connected = false;
      }
    } on SocketException catch (_) {
      _connected = false;
    } on TimeoutException catch (_) {
      _connected = false;
    }

    //The connection status changed send out an update to all listeners
    if (previousConnection != _connected) {
      _connectionChangeController.add(_connected);
    }

    return _connected;
  }

  Stream<bool> getConnectionStream() async* {
    yield await isConnected();
    await for (final bool? connected in _connectionChangeController.stream) {
      yield connected ?? false;
    }
  }

  bool? getLastConnectionsState() {
    return _connected;
  }
}

class NetworkConnectionException implements Exception {
  @override
  String toString() {
    return '>>> There is no Internet connection! <<<';
  }
}
