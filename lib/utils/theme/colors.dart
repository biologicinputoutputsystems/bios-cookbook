import 'package:flutter/material.dart';

class AppColors {
  static const MaterialColor primary = MaterialColor(
    0xFF0C5E98,
    {
      50: Color(0xFFafdeff),
      100: Color(0xFF68BFFD),
      200: Color(0xFF0C9BFA),
      300: Color(0xFF1587D9),
      400: Color(0xFF0C6DB1),
      500: Color(0xFF0C5E98),
      600: Color(0xFF0A5285),
      700: Color(0xFF074571),
      800: Color(0xFF053A5F),
      900: Color(0xFF043251),
    },
  );
  static const MaterialColor secondary = MaterialColor(
    0xFFB0BEC5,
    {
      50: Color(0xFFECEFF1),
      100: Color(0xFFCFD8DC),
      200: Color(0xFFB0BEC5),
      300: Color(0xFF90A4AE),
      400: Color(0xFF78909C),
      500: Color(0xFF607D8B),
      600: Color(0xFF546E7A),
      700: Color(0xFF455A64),
      800: Color(0xFF37474F),
      900: Color(0xFF263238),
    },
  );
  static const Color background = Colors.white;
  static Color text = secondary.shade900;
  static const Color success = Color(0xFF3BCF88);
  static const Color warning = Color(0xFFFFCD4E);
  static const Color error = Color(0xFFFF5B5B);
  static const Color none = Colors.grey;
  static const Color buttonSelected = Color(0xFF0A7BC7);
}
