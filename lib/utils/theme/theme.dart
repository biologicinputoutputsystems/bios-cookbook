import 'package:flutter/material.dart';

import 'colors.dart';

export 'bios_icons.dart';
export 'colors.dart';
export 'dimens.dart';

class AppFont {
  static const String ibmPlexSans = 'IBMPlexSans';
  static const String ibmPlexSansLight = 'IBMPlexSans-Light';
  static const String ibmPlexSansMedium = 'IBMPlexSans-Medium';
}

class AppTheme {
  static ThemeData get primary => ThemeData(
        primarySwatch: AppColors.primary,
        fontFamily: AppFont.ibmPlexSans,
        textTheme: const TextTheme(
          displayLarge: TextStyle(
            fontFamily: AppFont.ibmPlexSansLight,
            fontWeight: FontWeight.w300,
            height: 1.17,
            fontSize: 96.0,
            letterSpacing: -1.5,
          ),
          displayMedium: TextStyle(
            fontFamily: AppFont.ibmPlexSansLight,
            fontWeight: FontWeight.w300,
            height: 1.2,
            fontSize: 60.0,
            letterSpacing: -0.5,
          ),
          displaySmall: TextStyle(
            fontFamily: AppFont.ibmPlexSans,
            fontWeight: FontWeight.w400,
            height: 1.17,
            fontSize: 48.0,
          ),
          headlineMedium: TextStyle(
            fontFamily: AppFont.ibmPlexSans,
            fontWeight: FontWeight.w400,
            height: 1.06,
            fontSize: 34.0,
          ),
          headlineSmall: TextStyle(
            fontFamily: AppFont.ibmPlexSans,
            fontWeight: FontWeight.w400,
            fontSize: 24.0,
            letterSpacing: 0.18,
          ),
          titleLarge: TextStyle(
            fontFamily: AppFont.ibmPlexSansMedium,
            fontWeight: FontWeight.w500,
            height: 1.2,
            fontSize: 20.0,
            letterSpacing: 0.05,
          ),
          titleMedium: TextStyle(
            fontFamily: AppFont.ibmPlexSans,
            fontWeight: FontWeight.w400,
            height: 1.44,
            fontSize: 18.0,
            letterSpacing: 0.15,
          ),
          titleSmall: TextStyle(
            fontFamily: AppFont.ibmPlexSansMedium,
            fontWeight: FontWeight.w500,
            height: 1.71,
            fontSize: 14.0,
            letterSpacing: 0.1,
          ),
          bodyLarge: TextStyle(
            fontFamily: AppFont.ibmPlexSans,
            fontWeight: FontWeight.w400,
            height: 1.5,
            fontSize: 16.0,
          ),
          bodyMedium: TextStyle(
            fontFamily: AppFont.ibmPlexSansLight,
            fontWeight: FontWeight.w400,
            height: 1.43,
            fontSize: 14.0,
            letterSpacing: 0.25,
          ),
          labelLarge: TextStyle(
            fontFamily: AppFont.ibmPlexSansMedium,
            fontWeight: FontWeight.w600,
            height: 1.14,
            fontSize: 14.0,
            letterSpacing: 1.5,
          ),
          bodySmall: TextStyle(
            fontFamily: AppFont.ibmPlexSans,
            fontWeight: FontWeight.w400,
            height: 1.33,
            fontSize: 12.0,
            letterSpacing: 0.4,
          ),
          labelSmall: TextStyle(
            fontFamily: AppFont.ibmPlexSans,
            fontWeight: FontWeight.w600,
            height: 1.6,
            fontSize: 10.0,
            letterSpacing: 1.5,
          ),
        ),
      );
}
