/// Flutter icons BiosIcons
/// Copyright (C) 2022 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  BiosIcons
///      fonts:
///       - asset: fonts/BiosIcons.ttf
///
/// 
///
import 'package:flutter/widgets.dart';

class BiosIcons {
  BiosIcons._();

  static const String _kFontFam = 'BiosIcons';
  static const String? _kFontPkg = null;

  static const IconData accuracy = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData api = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData core_execution_layer = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData data_loader = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData data = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData degrees_of_freedom = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData error = IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData export_data = IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData latency = IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData model = IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData pause = IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData play = IconData(0xe80b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData replay = IconData(0xe80c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData stop = IconData(0xe80d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData success = IconData(0xe80e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
