class Dimens {
  static const double margin0 = 0;
  static const double margin1 = 4;
  static const double margin2 = 8;
  static const double margin3 = 12;
  static const double margin4 = 16;
  static const double margin5 = 24;
  static const double margin6 = 32;
  static const double margin7 = 48;
  static const double margin8 = 54;
  static const double cornerRadius = 4;
  static const double modalCornerRadius = 16;
  static const double buttonCornerRadius = 32;
}
