import 'package:grpc/grpc.dart';

class LoggerInterceptor implements ClientInterceptor {
  const LoggerInterceptor({
    this.showRequest = true,
    this.showResponse = true,
    this.logPrint = print,
  });

  final bool showRequest;
  final bool showResponse;
  final Function(Object object) logPrint;

  @override
  ResponseFuture<R> interceptUnary<Q, R>(
    ClientMethod<Q, R> method,
    Q request,
    CallOptions options,
    ClientUnaryInvoker<Q, R> invoker,
  ) {
    // var newOptions = options.mergedWith(CallOptions(metadata: <String, String>{
    //   'token': 'Some-Token',
    // }));

    if (showRequest) {
      logPrint('>>>>>>  ${method.path}  >>>>>>');
      logPrint('\n');
      logPrint('$request');
      logPrint('\n');
      logPrint('>>>>>>>>>>>>>>>>>>>>>>>>');
    }

    final ResponseFuture<R> response = invoker(method, request, options);

    if (showResponse) {
      response.then((r) {
        logPrint('<<<<<<  ${method.path}  <<<<<<');
        logPrint('\n');
        logPrint('$r');
        logPrint('\n');
        logPrint('<<<<<<<<<<<<<<<<<<<<<<<<');
      });
    }

    return response;
  }

  @override
  ResponseStream<R> interceptStreaming<Q, R>(
    ClientMethod<Q, R> method,
    Stream<Q> requests,
    CallOptions options,
    ClientStreamingInvoker<Q, R> invoker,
  ) {
    if (showRequest) {
      logPrint('>>>>>>  ${method.path}  >>>>>>');
    }
    return invoker(method, requests, options);
  }
}
