abstract class ICsvFile<T> {
  List<String> get header;

  int get rowCount;

  String get filePath;

  bool get isLoaded;

  List<T> operator [](int index);

  Future<void> loadAsync();

  void loadSync();
}
