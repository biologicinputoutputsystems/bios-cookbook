import 'dart:io';

import 'package:csv/csv.dart';

import 'csv_file_interface.dart';

class CsvFile<T> implements ICsvFile<T> {
  CsvFile(this.filePath);

  List<List<dynamic>>? _data;

  @override
  String filePath;

  @override
  int get rowCount => _data != null ? _data!.length : 0;

  @override
  bool get isLoaded => _data != null;

  @override
  List<String> get header => _data != null ? _data![0].map((dynamic e) => e.toString()).toList() : <String>[];

  @override
  Future<void> loadAsync() async {
    final File file = File(filePath);
    final String stringData = await file.readAsString();

    _data = const CsvToListConverter().convert<dynamic>(stringData);
  }

  @override
  void loadSync() {
    final File file = File(filePath);
    final String stringData = file.readAsStringSync();

    _data = const CsvToListConverter().convert<dynamic>(stringData);
  }

  @override
  List<T> operator [](int index) {
    return _data![index].map((dynamic e) => e as T).toList();
  }
}
