import 'dart:async';

import 'package:bios_cookbook/data/fs/csv/csv_file.dart';
import 'package:bios_cookbook/domain/extensions/bone_rotation_data_extensions.dart';
import 'package:bios_cookbook/domain/models/axis.dart';
import 'package:bios_cookbook/domain/models/bone_rotation_data.dart';
import 'package:bios_cookbook/domain/models/kinematic_data.dart';
import 'package:bios_cookbook/domain/models/kinematic_data_info.dart';

import 'csv/csv_file_interface.dart';

class _KinematicDataMapping {
  static Map<String, String> boneNames = <String, String>{
    'thumb flex/ext': 'RHandThumb',
    'index flex/ext': 'RHandIndex',
    'middle flex/ext': 'RHandMiddle',
    'ring flex/ext': 'RHandRing',
    'pinky flex/ext': 'RHandPinky',
    'wrist flex/ext': 'RHand',
    'thumb ad/abd': 'RHandThumb',
    'index ad/abd': 'RHandIndex',
    'middle ad/abd': 'RHandMiddle',
    'ring ad/abd': 'RHandRing',
    'pinky ad/abd': 'RHandPinky',
    'wrist deviation': 'RHand',
    'wrist pro/sup': 'RHand',
  };

  static Map<String, Axis> targetAxis = <String, Axis>{
    'thumb flex/ext': Axis.Y,
    'index flex/ext': Axis.Y,
    'middle flex/ext': Axis.Y,
    'ring flex/ext': Axis.Y,
    'pinky flex/ext': Axis.Y,
    'wrist flex/ext': Axis.Y,
    'thumb ad/abd': Axis.X,
    'index ad/abd': Axis.X,
    'middle ad/abd': Axis.X,
    'ring ad/abd': Axis.X,
    'pinky ad/abd': Axis.X,
    'wrist deviation': Axis.X,
    'wrist pro/sup': Axis.Z,
  };
}

class KinematicDataReader {
  const KinematicDataReader();

  int _millisecondsToNextFrame(ICsvFile<double> kinematicData, int position) {
    if (position == kinematicData.rowCount - 1) {
      return 33;
    }

    final double nextTimestamp = kinematicData[position + 1].first;
    final double currentTimestamp = kinematicData[position].first;

    return ((nextTimestamp - currentTimestamp) * 1000).toInt();
  }

  KinematicDataInfo dataInfo(String csvFilePath) {
    return KinematicDataInfo.fromCsvFile(csvFilePath);
  }

  Stream<KinematicData> asStream(
    String csvFilePath, {
    int position = 1,
    int movementPosition = 0,
  }) async* {
    final ICsvFile<double> kinematicData = CsvFile<double>(csvFilePath);
    await kinematicData.loadAsync();

    final int length = kinematicData.rowCount - 1;
    final double durationInSeconds = kinematicData[length].first;
    int streamPosition = position.clamp(1, length);

    while (streamPosition < length + 1) {
      final Iterable<BoneRotationData> rotationData = _readLine(
        kinematicData,
        streamPosition,
      );

      yield KinematicData(
        rotationData,
        streamPosition / length,
        durationInSeconds,
        movementPosition,
      );

      await Future<void>.delayed(
        Duration(
          milliseconds: _millisecondsToNextFrame(kinematicData, streamPosition),
        ),
      );

      streamPosition++;
    }
  }

  Iterable<BoneRotationData> _readLine(
      ICsvFile<double> kinematicData, int position) {
    final List<String> header = kinematicData.header.skip(1).toList();
    final List<double> dataRow = kinematicData[position].skip(1).toList();

    final Iterable<BoneRotationData> result = dataRow
        .asMap()
        .entries
        .map((MapEntry<int, double> e) {
          if (!_KinematicDataMapping.boneNames.containsKey(header[e.key])) {
            return BoneRotationData('');
          }

          final String boneName =
              _KinematicDataMapping.boneNames[header[e.key]]!;
          final Axis targetAxis =
              _KinematicDataMapping.targetAxis[header[e.key]]!;

          return BoneRotationData.fromTargetAxis(boneName, targetAxis, e.value);
        })
        .where((BoneRotationData element) => element.name != '')
        .joinByBoneName();

    return result;
  }
}
