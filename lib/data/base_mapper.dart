abstract class Mapper<A, B> {
  B fromEntity(A entity);

  A toEntity(B item);
}

class BaseListMapper<A, B> implements Mapper<List<A>, List<B>> {
  const BaseListMapper(this.mapper);

  final Mapper<A?, B?> mapper;

  @override
  List<B> fromEntity(List<A>? entityList) {
    final List<B> list = [];

    if (entityList == null || entityList.isEmpty) {
      return list;
    }

    for (final A entity in entityList) {
      final B? mapped = mapper.fromEntity(entity);
      if (mapped != null) {
        list.add(mapped);
      }
    }
    return list;
  }

  @override
  List<A> toEntity(List<B>? itemList) {
    final List<A> list = [];

    if (itemList == null || itemList.isEmpty) {
      return list;
    }

    for (final B item in itemList) {
      final A? mapped = mapper.toEntity(item);
      if (mapped != null) {
        list.add(mapped);
      }
    }
    return list;
  }
}
