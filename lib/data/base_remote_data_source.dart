import 'dart:async';
import 'dart:io';

import 'package:bios_cookbook/bios_cookbook.dart';
import 'package:dio/dio.dart';
import 'package:grpc/grpc.dart';

abstract class BaseRemoteDataSource {
  const BaseRemoteDataSource(this.connectionUtils);

  final ConnectionUtils connectionUtils;

  Future<T> execute<T>(Future<T> Function() apiCall) async {
    final bool isConnected = await connectionUtils.isConnected();
    if (isConnected) {
      try {
        return apiCall.call().catchError((Object e, StackTrace stackTrace) {
          throw _handleError(e);
        });
      } catch (e) {
        throw _handleError(e);
      }
    } else {
      throw ConnectionException();
    }
  }

  Stream<T> executeStream<T>(Stream<T> Function() apiCall) async* {
    final bool isConnected = await connectionUtils.isConnected();
    if (isConnected) {
      try {
        yield* apiCall.call().handleError((Object e, StackTrace stackTrace) {
          throw _handleError(e);
        });
      } catch (e) {
        throw _handleError(e);
      }
    } else {
      throw ConnectionException();
    }
  }

  AppError _handleError(Object e) {
    logger.e(e);

    // non-200 error goes here.
    if (e is GrpcError) {
      throw _handleGrpcError(e);
    } else if (e is DioException) {
      throw _handleDioError(e);
    } else if (e is SocketException || e is TimeoutException) {
      throw ConnectionException();
    } else if (e is HttpException) {
      if (e.message
          .startsWith('Connection closed before full header was received')) {
        throw ConnectionException();
      }
      throw UnknownException();
    } else {
      throw UnknownException();
    }
  }

  AppError _handleGrpcError(GrpcError error) {
    switch (error.code) {
      case StatusCode.unavailable:
        return ConnectionException();
      case StatusCode.cancelled:
        return SilentException();
      case StatusCode.unauthenticated:
        return UnauthorizedException();
      case StatusCode.notFound:
        if ((error.message?.contains('Session') ?? false) &&
            (error.message?.contains('not active') ?? false)) {
          return UnauthorizedException();
        }
        return UnknownException();
      case StatusCode.unknown:
      default:
        if (error.rawResponse is SocketException ||
            error.rawResponse is TimeoutException ||
            (error.message?.startsWith(
                    'SocketException: OS Error: Network is unreachable') ??
                false)) {
          return ConnectionException();
        }
        return UnknownException();
    }
  }

  AppError _handleDioError(DioException error) {
    switch (error.type) {
      case DioExceptionType.connectionTimeout:
      case DioExceptionType.connectionError:
      case DioExceptionType.sendTimeout:
      case DioExceptionType.receiveTimeout:
        return ConnectionException();
      case DioExceptionType.cancel:
        return SilentException();
      case DioExceptionType.badResponse:
      case DioExceptionType.unknown:
      default:
        if (error.error is SocketException ||
            error.error is TimeoutException ||
            error.message?.startsWith(
                    'SocketException: OS Error: Network is unreachable') ==
                true) {
          return ConnectionException();
        } else if (error.response?.statusCode == 401) {
          return UnauthorizedException();
        }
        return UnknownException();
    }
  }
}
